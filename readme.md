Date Added
==========

- Jupyter notebook extension
- Display when each cell was added
- Useful e.g. when using Jupyter notebooks as a scientific research record


Installation
============

From a suitable place on your system:

    git clone https://gitlab.com/steve_biggs/date_added.git
    jupyter nbextension install date_added --user


Credit / License
================

**NB:** A lot of this is based on:
https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tree/master/src/jupyter_contrib_nbextensions/nbextensions/execute_time

For everything else:

Copyright 2017 Steve Biggs

date_added is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

date_added is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with date_added.  If not, see http://www.gnu.org/licenses/.
